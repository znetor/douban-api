#coding:utf-8
import  requests
import urllib

API_URL = 'https://api.douban.com/v2/'
AUTH_URL = 'https://www.douban.com/service/auth2/auth'
TOKEN_URL = 'https://www.douban.com/service/auth2/token'

class BOOK_NOT_FOUND(Exception): pass
class APIError(StandardError): pass # 调用未知API

class APIClient(object):
    def __init__(self, api_key, secret, redirect_url, response_type, scope=(), state=None):
        self.__client_id = api_key
        self.__secret = secret
        self.__redirect_url = redirect_url
        self.__response_type = response_type
        self.__scope = scope
        self.__state = state
        self.__headers = {}

    def __getattr__(self, attr):
        '''当访问一个对象中没有的属性时， __getattr__() 会被调用'''
        l = attr.split('__')
        method = l.pop(0)
        if not method in ('get', 'post', 'put', 'delete'):
            raise AttributeError
        for index, val in enumerate(l):
            if val in ['id', 'name']:
                l[index] = '%s'
        # 返回一个函数，处理调用参数
        def wrapper(*args, **kwargs):
            path = API_URL + '/'.join(l)
            print path
            path = path % args
            if method == 'get':
                r = requests.get(path,headers=self.__headers)
                self._check_exception(r)
                print r.url
                data = r.json()
                if data.get('code') == 6000:
                    raise BOOK_NOT_FOUND
                return data
        return wrapper

    @property
    def authorize_url(self):
        param_dict = {
                'client_id': self.__client_id,
                'redirect_uri': self.__redirect_url,
                'response_type': self.__response_type
                }
        if self.__scope:
            param_dict['scope'] = ','.join(self.__scope)
        if self.__state:
            param_dict['state'] = self.__state
        param = urllib.urlencode(param_dict)
        return "%s?%s" % (AUTH_URL, param)

    def gen_access_token(self, code):
        param_dict = {
                'client_id': self.__client_id,
                'client_secret': self.__secret,
                'redirect_uri': self.__redirect_url,
                'grant_type': 'authorization_code',
                'code': code
                }
        r = requests.post(TOKEN_URL, data=param_dict)
        ret = r.json()
        if not r.status_code == 200:
            raise APIError
        self.__access_token = ret['access_token']
        self.__expires_in = ret['expires_in']
        self.__refresh_token = ret['refresh_token']
        self.__douban_user_id = ret['douban_user_id']
        self.__headers['Authorization'] = 'Bearer %s' % (self.__access_token)
        return ret

    def _check_exception(self, obj):
        if obj.status_code >= 400:
            raise APIError
